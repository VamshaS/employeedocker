FROM openjdk:17
COPY target/mysql-docker.jar /mysql-docker.jar
EXPOSE 8080 
ENTRYPOINT [ "java","-jar","/mysql-docker.jar" ]