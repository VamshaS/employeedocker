package com.example.apiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerMysql1Application {

	public static void main(String[] args) {
		SpringApplication.run(DockerMysql1Application.class, args);
	}

}
