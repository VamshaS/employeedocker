package com.example.apiservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.apiservice.entity.Employee;
import com.example.apiservice.repository.EmployeeRepository;

@RestController
public class MyController {
	@Autowired
	private EmployeeRepository employeeRepository;
	@PostMapping("/employees")
	public Employee saveEmployee(@RequestBody Employee employee) {
		 return  employeeRepository.save(employee);
	}
	@GetMapping("/employees")
	public List<Employee> getEmployees(){
		return employeeRepository.findAll();
	}
}
