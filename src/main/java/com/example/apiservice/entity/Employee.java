package com.example.apiservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Employee {
	@Id
	private String sap;
	private String name;
	private Integer age;
	public String getSap() {
		return sap;
	}
	public void setSap(String sap) {
		this.sap = sap;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Employee(String sap, String name, Integer age) {
		super();
		this.sap = sap;
		this.name = name;
		this.age = age;
	}
	public Employee() {
		super();
	}
	
	
}
